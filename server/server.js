const express = require('express'),
    cors = require('cors'),
    server = express(),
    mongoose = require('mongoose'),
    usersRoutes = require('./routes/usersRoute.js'),
    activitiesRoutes = require('./routes/activitiesRoute')
    interestsRoutes = require('./routes/interestsRoute.js'),
    bodyParser = require('body-parser'),
    jwt = require('jsonwebtoken'),
    mongoConfig = require('./mongoConfig');

var port = process.env.port || 3250;

// kidsRoutes = require('./routes/kids.js'),
// activitiesRoutes = require('./routes/activities.js'),
// addressesRoutes = require('./routes/addresses.js'),
// bookingsRoutes = require('./routes/booings.js'),
// commentsRoutes = require('./routes/comments.js'),
// interestsRoutes = require('./routes/interests.js'),

// ‘newdatabase’ is an arbitrary name for your DB’s instance name
// We can have multiple DB's instances with different names
server.use(cors());
// =================== initial settings ===================
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());
// connnect to mongo

// connecting to mongo and checking if DB is running
async function connecting() {
    try {
        await mongoose.connect(mongoConfig.localMongoURL, { useUnifiedTopology: true, useNewUrlParser: true })
        console.log('Connected to the DB')
    } catch (error) {
        console.log('ERROR: Seems like your DB is not running, please start it up !!!');
    }
}
connecting()
// end of connecting to mongo and checking if DB is running
// temp stuff to suppress internal warning of mongoose which would be updated by them soon
mongoose.set('useCreateIndex', true);
// end of connecting to mongo and checking if DB is running

// routes
server.use('/kidder', activitiesRoutes);
server.use('/kidder', usersRoutes);
server.use('/kidder', interestsRoutes);
// Set the server to listen on port 3250
server.listen(port, () => console.log(`listening on portTT ${port}`))