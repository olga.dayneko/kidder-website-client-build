const router = require('express').Router();
const controller = require('../controllers/interestsController.js');

router.get('/interests/all', controller.getAllInterests);
router.post('/interests/add', controller.addInterest);
/*router.delete('/interests/remove', controller.removeInterest);
router.get('/interests/one', controller.getOneInterest);
router.post('/interests/registration', controller.register);
router.post('/interests/login', controller.login);
router.post('/interests/verify_token', controller.verify_token);
router.post('/interests/update', controller.updateInterest);*/

module.exports = router;