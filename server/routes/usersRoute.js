const router = require('express').Router();
const controller = require('../controllers/usersController.js');

//for admin panel
router.post('/users/add', controller.addUser);
router.delete('/users/remove', controller.removeUser);
router.get('/users/all', controller.getAllUsers);
router.get('/users/one', controller.getOneUser);
router.post('/users/registration', controller.register);
router.post('/users/login', controller.login);
router.post('/users/verify_token', controller.verify_token);
router.post('/users/update', controller.updateUser);



module.exports = router;