var express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const config = require('../config');
const Activities = require('../schemas/activitiesSchema')
const mongoose = require('mongoose');

class ActivitiesController {
    // Route to POST ADD one activity
    
    async addActivity(req, res) {
        let { name, business, location, interests, languages, description, duration, schedule, pictures, comments } = req.body;
        try {
            const activityAdded = await Activities.create({ name, business, location, interests, languages, description, duration, schedule, pictures, comments });
            res.send(activityAdded);
        }
        catch (error) {
            console.log(error);
            res.send({ error });
        }
    }

    // Route to REMOVE activity
    async removeActivity(req, res) {
        let { _id } = req.body;
        try {
            const activityRemoved = await Activities.deleteOne({ _id });
            res.send({ activityRemoved });
        } catch (error) {
            res.send({ error });
        }
    }
    //Route to UPDATE activity
    async updateActivity(req, res) {
        let { _id, name, business, location, interests, languages, description, duration, scheule, pictures, comments } = req.body;
        try {
            const objForUpdate = {}
            for (var key in req.body) {
                if (key !== '_id' && req.body.key !== null && req.body[key] !== "") {
                    objForUpdate[key] = req.body[key]
                }
            }
            const activityUpdated = await Activities.updateOne({ _id }, objForUpdate);
            res.send({ updated: activityUpdated });
        } catch (error) {
            res.send({ error });
        }
    }
    //Route to GET all activities
    async getAllActivities(req, res) {
        try {
            const activities = await Activities.find({});
            res.send(activities);
        } catch (error) {
            res.send({ error });
        }
    }

    // GET one activity
    async getOneActivity(req, res) {
        let { _id } = req.body;
        try {
            const activity = await Activities.findOne({ _id });
            res.send({ activity });
        } catch (error) {
            res.send({ error });
        }
    }

    async findActivities(req, res) {
        // let { name, location, interests, languages } = req.body;
        var interestsToObjects = [];
        //console.log("req.body.interests: " ,req.body.interests.length);
        let { interests } = req.body;
        for (var i=0; i<req.body.interests.length; i++){
            interestsToObjects.push(interests[i]);
        }
        //console.log("interestToObj: ", interestsToObjects);
        interests = interestsToObjects;
        try {
            // const activities = await Activities.find({ "name":{$in:name},"interests":{$in:interests}, "name":{$in:interests}, "languages": {$in:languages}, "description":{$in:description}, "location":{$in:location}});
            const activities = await Activities.find( { interests: {$in: interestsToObjects }   });
            
            res.send(activities);
            console.log() 
        } catch (error) {
            res.send({ error });
        }
    }
}

module.exports = new ActivitiesController();
