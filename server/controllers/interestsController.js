var express = require('express');
const Interests = require('../schemas/interestsSchema');


class InterestsController {

    async getAllInterests(req, res) {
        try {
            console.log('============')
            const interests = await Interests.find({});
            res.send(interests);
        } catch (error) {
            console.log(error);
            res.send({ error });
            
        }
    }
    async addInterest(req, res) {
        let { name } = req.body;
        try {
            const result = await Interests.create({ name })
            res.send(result);
        } catch (error) {
            res.send(error);
        }
    }
}

module.exports = new InterestsController;