var express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const config = require('../config');
const Kids = require('../schemas/kidsSchema')



class KidsController {
    // Route to POST ADD one kid
    async addKid(req, res) {
        let { name, lastname, age, gender, user, interests, languages, profile_picture } = req.body;
        try {
            const kidAdded = await Kids.create({ name, lastname, age, gender, user, interests, languages, profile_picture });
            res.send(kidAdded);
        }
        catch (error) {
            console.log(error);
            res.send({ error });
        }
    }

    // Route to REMOVE kid
    async removeKid(req, res) {
        let { _id } = req.body;
        try {
            const kidRemoved = await Kids.deleteOne({ _id });
            res.send({ kidRemoved });
        } catch (error) {
            res.send({ error });
        }
    }
    //Route to UPDATE user
    async updateKid(req, res) {
        let { _id, name, lastname, age, gender, user, interests, languages, profile_picture } = req.body;
        try {
            const objForUpdate = {}
            for (var key in req.body) {
                if (key !== '_id' && req.body.key !== null && req.body[key] !== "") {
                    objForUpdate[key] = req.body[key]
                }
            }
            const kidUpdated = await Kids.updateOne({ _id }, objForUpdate);
            res.send({ updated: kidUpdated });
        } catch (error) {
            res.send({ error });
        }
    }
    //Route to GET all kids
    async getAllKids(req, res) {
        try {
            const kids = await Kids.find({});
            res.send(kids);
        } catch (error) {
            res.send({ error });
        }
    }

    // GET one kid
    async getOneKid(req, res) {
        let { _id } = req.body;
        try {
            const kid = await Kids.findOne({ _id });
            res.send({ kid });
        } catch (error) {
            res.send({ error });
        }
    }
}

module.exports = new KidsController();
