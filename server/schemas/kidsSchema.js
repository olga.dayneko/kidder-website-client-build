import { Schema as _Schema, model } from 'mongoose';
const Schema = _Schema;
const ObjectId = Schema.Types.ObjectId; // here we can change ObjectID to ObjectName?
const kidsSubschema = new Schema({
    name: {
        type: String,
        unique: true,
        required: "Please enter name!"
    },
    lastname: { // not sure we need to have this here? we may need it to be able to book an appointment, or not?
        type: String,
        unique: true,
        required: "Please enter lastname!"
    },
    age: {
        type: Number,
        required: "Please enter child age!"
    },
    gender: {
        type: String,
        enum: ['Male', 'Female', 'Other'] //other would not filter by gender
    },
    user: { // should we cross-reference both uses/kids DBs or we can just have kids linked to user ids??
        type: ObjectId,
        ref: 'user',
        required: true
    },
    interests: [{
        type: ObjectId,
        ref: 'interest',
        required: false
    }],
    languages: [{
        type: String
    }],
    profile_picture: { type: URL },
});
export default model('kid', kidsSubschema);