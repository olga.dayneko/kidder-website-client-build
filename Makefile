.PHONY: clean
clean: docker_rmi ## Stop running containers, remove containers and images
docker_rmi: docker_rm
	@images=`docker images | grep kidder | awk '{print $$3}'`;
	@if [ -n "${images}" ]; then \
		docker rmi ${images} > /dev/null 2>&1; \
	fi
docker_rm: docker_stop
	@stopped=`docker ps -a | grep website | awk '{print $$1}'`;
	@if [ -n "${stopped}" ]; then \
		docker rm ${stopped} > /dev/null 2>&1; \
	fi
docker_stop:
	@if ! [ -x "$$(command -v docker)" ]; then \
		echo "Docker is missing"; \
		exit 0; \
	elif ! [ -x "$$(command -v docker-compose)" ]; then \
		echo "Docker-compose is missing"; \
		exit 0; \
	fi
	@docker-compose down;
	@running=`docker ps | grep website | awk '{print $$1}'`;
	@if [ -n "${running}" ]; then \
		docker stop ${running} > /dev/null 2>&1; \
	fi


.PHONY: run
run: lint ## Build images, create containers and run the App locally
	@docker-compose up --build


testALL: lint testHTML testJS

testHTML: ## Test HTML valididty
	@docker run --rm -v `pwd`/:/mnt 18fgsa/html-proofer mnt --disable-external

testJS: ## Test JS validity
	@docker run -it --rm -v `pwd`/:/code eeacms/jslint --white --vars --regexp --color /code/**/*.js

lint: ## Prepare source files for build

	@ # Replace tabs with spaces in JS files
	@find . -type f -name '*.js' | xargs sed -i 's/\t/    /g';

	@ # Replace tabs with spaces in CSS files
	@find . -type f -name '*.css' | xargs sed -i 's/\t/    /g';

	@ # Replace localhost and/or 127.0.0.1 with DB service name [or IP] from mongoConfig
	@sed -i -E 's/localhost|127\.0\.0\.1/db/g' server/mongoConfig.js;

	@ # Replace localhost and/or 127.0.0.1 with SERVER service name [or IP]
	@find client/. -type f -print0 | xargs -0 sed -i -E 's/localhost|127\.0\.0\.1/server/g';


.PHONY: help
help: ## Show usage help
	@echo "Valid targets:"
	@grep -E '^[a-zA-Z_-]+:.?*## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-10s\033[0m %s\n", $$1, $$2}'
