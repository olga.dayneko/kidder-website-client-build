import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import Registration from './Authentification/components/Registration/Registration';
import Login from './Authentification/components/Login/Login';
import UserAccount from './UserAccount/components/Account/Account'
import Navbar from './Header/components/Navbar'
import FindYourActivity from './FindActivity/components/FindYourActivity';
import { Helmet } from "react-helmet";
import Axios from 'axios';
import { useEffect, useState } from 'react';
import { URL } from './config';

function App() {
   const [interests, setInterests] = useState([])
   useEffect(() => {
      getInterests();
   }, []);

   const getInterests = async () => {
      try {

         const response = await Axios.get(`${URL}/kidder/interests/all`);
         setInterests([...response.data]);
      }
      catch (error) { }
   }
   return (

      <Router>
         <Helmet>
            <title>Amazing website</title>
            <meta charSet="utf-8" />
            <meta name="description" content="Example implementation of Stripe checkout with React.js" />
            <meta name='keywords' content='very cool kids website' />
            <meta name='viewport' content='width=device-width, initial-scale=1' />
         </Helmet>
         <Navbar />
         <Route exact path='/' render={(props) => { return <FindYourActivity {...props} interests={interests} /> }} />
         <Route path='/registration' component={Registration} />
         <Route path='/login' component={Login} />
         <Route path='/account' render={(props) => { return <UserAccount {...props} interests={interests} /> }} />
      </Router>

   )
}
export default App;
