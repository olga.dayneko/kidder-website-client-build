import React, { useState } from 'react';
import Axios from 'axios';
import {URL} from '../../../config'

const Registration = () => {
    const [form, setValues] = useState({
        type:"business",
        firstname: "",
        lastname: "",
        email: "",
        password: "",
        confirm_password: ""
    });
    const [message, setMessage] = useState('');
    const handleChange = e => {
        setValues({ ...form, [e.target.name]: e.target.value })
    }
    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            const response = await Axios.post(`${URL}/kidder/users/registration`, {
                type: form.type,
                firstname: form.firstname,
                lastname: form.lastname,
                email: form.email,
                password: form.password,
                confirm_password: form.confirm_password
            })
            setMessage(response.data.message)
        }
        catch (error) {
            console.log(error)
        }
    }

    return <form onSubmit={handleSubmit} onChange={handleChange} id="userRegistrationForm" className="userRegistrationForm">
        <label>First name:</label>
        <input id="firstname" type="text" name="firstname" />
        <label>Last name:</label>
        <input id="lastname" type="text" name="lastname" />
        <label>Email:</label>
        <input id="email" type="email" name="email" />
        <label>Password:</label>
        <input id="password" type="password" name="password" />
        <label>Confirm password:</label>
        <input id="confirm_password" type="password" name="confirm_password" />
        <button>Register</button>
        <div className='message'><h4>{message}</h4></div>
    </form>
}
export default Registration;