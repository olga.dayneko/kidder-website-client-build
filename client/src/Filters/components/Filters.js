import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import Tooltip from 'rc-tooltip';
import 'rc-slider/assets/index.css';
import Slider, { Range } from 'rc-slider';
import Combobox from 'react-widgets/lib/Combobox'
import Multiselect from 'react-widgets/lib/Multiselect'
import { getItem } from '../../General/generalMethods'
import 'react-widgets/dist/css/react-widgets.css'
import { URL } from '../../config.js'
import { getIndexByAttribute } from './../../General/generalMethods'


function Filters(props) {

    let languages = ['English', 'French', 'Spanish', 'German'];
    let gender = ['Boy', 'Girl', 'Other'];
    var selectedInterestsIDs = [];
    const [form, setValues] = useState({
        keywords: "",
        age: "",
        gender: "",
        interests: "",
        languages: "",
        duration: "",
        time: ""
    });
    const [interests, setInterests] = useState(props.interests);
    const [searchResults, setSearchResults] = useState([]);
    const Handle = Slider.Handle;
    var [message, setMessage] = useState('');
    const [selectedInterests, setSelectedInterests] = useState([]);

    const onChangeInterests = (value) => {
        setSelectedInterests(value);
    }

    const getInterestsIDs = () => {
        var interestsFromProps = props.interests;
        for (var i = 0; i < selectedInterests.length; i++) {
            var index = getIndexByAttribute(interestsFromProps, 'name', selectedInterests[i])
            selectedInterestsIDs.push(interestsFromProps[index]._id)
        }
        return selectedInterestsIDs;
    }


    useEffect(() => {
        const interestsList = [];
        props.interests.forEach(element => interestsList.push(element.name))
        setInterests([...interestsList]);
        // eslint-disable-next-line react-hooks/exhaustive-deps  
    }, [props.interests]);


    var defaultAgeRange = [3, 6];
    var [ageRange, setAgeRange] = useState(defaultAgeRange);

    var defaultDurationRange = [1, 3];
    var [durationRange, setDurationRange] = useState(defaultDurationRange);

    var defaultTimeRange = [12, 16];
    var [timeRange, setTimeRange] = useState(defaultTimeRange);

    const handle = (props) => {
        const { value, dragging, index, ...restProps } = props;
        return (
            <Tooltip
                prefixCls="rc-slider-tooltip"
                overlay={value}
                visible={dragging}
                placement="top"
                key={index}
            >
                <Handle value={value} {...restProps} />
            </Tooltip>
        );
    };

    const handleChange = e => {
        setValues({ ...form, [e.target.name]: e.target.value })
    }
    const handleSubmit = async (e) => {
        e.preventDefault()
        getInterestsIDs();
        // console.log("selectedInterestsIDs: " + selectedInterestsIDs)
        try {
            const searchedActivities = await Axios.post(`${URL}/kidder/activities/find`, {
                interests: selectedInterestsIDs
            })
            // console.log("this is data we are sending on submit: " + JSON.stringify(response));
            setMessage(searchedActivities.data.message);
            setSearchResults(searchedActivities);
           // console.log("searchResults are here: ", searchedActivities);
            props.getResults(searchedActivities.data)
        }
        catch (error) {
            console.log(error)
        }
    }
    var convertTime = (time) => {
        var hours = [];
        var minutes = [];
        for (var i = 0; i < time; i++) {
            hours += Math.floor((ageRange[i]) / 60);
            minutes += ageRange[i] % 60;
        }
        return `${hours}:${minutes}`;
    }



    return <form onSubmit={handleSubmit} onChange={handleChange} id="filtersForm" className='filtersForm'>

            <label>Search text: </label>
            <input id="searchBar" className='searchBar' name="searchBar" defaultValue='sewing class for kids in barcelona'></input>
            <label>Age: {` ${ageRange[0]} - ${ageRange[1]} years old`}</label>
            <div>
                <Range dots={false}
                    handle={handle}
                    step={1}
                    min={0}
                    max={18}
                    marks={{ 0: 0, 18: 18 }}
                    //pushable={true}
                    onChange={(value) => { setAgeRange([...value]); }}
                    defaultValue={defaultAgeRange} />
            </div>
            <br />
            <label>Gender:</label>
            <Combobox id="gender"
                name="gender"
                data={gender}
                defaultValue={""}
            />
            <label>Interests:</label>
            <Multiselect id="interests"
                name='interests'
                onChange={value => onChangeInterests(value)}
                data={interests}
                allowCreate={true}
                textField='name'
                caseSensitive={false}
                minLength={2}
                filter='contains'
            />
            <label>Languages:</label>
            <Multiselect id="languages"
                name='languages'
                data={languages}
                defaultValue={languages[0]}
                textField='language'
                caseSensitive={false}
                minLength={2}
                filter='contains'
            />
            <label>Duration: {` ${durationRange[0]} - ${durationRange[1]} hours`}</label>

            <div>
                <Range dots={false}
                    name='duration'
                    id='duration'
                    handle={handle}
                    step={0.5}
                    min={0}
                    max={8}
                    marks={{ 0: 0, 8: 8 }}
                    onChange={(value) => { setDurationRange([...value]); }}
                    defaultValue={defaultDurationRange} />
            </div>
            <br />
            <label>Time: {`${timeRange[0]} - ${timeRange[1]}`}</label>
            <div>
                <Range dots={false}
                    name='time'
                    id='time'
                    handle={handle}
                    step={0.5}
                    min={8}
                    max={21}
                    marks={{ 8: '8:00', 21: '21:00' }}
                    onChange={(value) => { setTimeRange([...value]); }}
                    defaultValue={defaultTimeRange} />
            </div>
            <br />
            <button>Search</button>
            <div className='message'><h4>{message}</h4></div>
        </form>
}
export default Filters;