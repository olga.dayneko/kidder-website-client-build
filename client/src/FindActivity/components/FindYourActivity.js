import React , {useState}from 'react';
import Filters from '../../Filters/components/Filters';
import Results from './Results';

const FindYourActivity = (props) => {
     const [results,setResults] = useState([])

     const getResults = (results) => {
        setResults([...results])
     }

    return <div id="findYourActivity" class="findYourActivity wrapper">
        <Filters className='filtersForm' interests={props.interests} getResults={getResults}/>
        <Results className='results' results={results}/>
    </div>
}

export default FindYourActivity;