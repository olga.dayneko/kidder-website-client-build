import React, { useState, useEffect } from 'react';
import Axios from 'axios';

const Results = (props) => {
   
    
    console.log("these are results from props: ", props.results);

    return <div className='results align-content-start flex-wrap' > Your search results are: 
            {
                props.results.map((ele, i) => {
                    return <div class="p-2 bd-highlight" key={i}>{ele.name}</div>
                })
            }
    </div>

}

export default Results;