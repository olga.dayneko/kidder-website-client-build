import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import { getIndexByAttribute } from './../../../General/generalMethods'
import Filters from '../../../Filters/components/Filters';
import {URL} from '../../../config'

const ChildProfile = (props) => {

    const [form, setValues] = useState({
        name: "",
        location: "",
        description: "",
        pictures: "",
        business: "5de7dd3ae4669821b72e7940",
        interests: '',
        languages: '',
        duration: '',
        schedule: ''
    });
    const [interests, setInterests] = useState(props.interests);
    const [interestsSelected, setInterestsSeleted] = useState([]);
    const [message, setMessage] = useState('');
    var interestsIDs = [];
    const handleChange = e => {
        setValues({ ...form, [e.target.name]: e.target.value })
    }

    const onChangeInterests = (value) => {
        setInterestsSeleted(value);
    }

    const getInterestsIDs = () => {
        var interestsFromProps = props.interests;
        for (var i = 0; i < interestsSelected.length; i++) {
            var index = getIndexByAttribute(interestsFromProps, 'type', interestsSelected[i])
            interestsIDs.push(interestsFromProps[index]._id)
        }
        return interestsIDs;
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        getInterestsIDs();
        try {
            const response = await Axios.post(`${URL}/kidder/activities/add`, {
                name: form.activityname,
                business: "5de7dd3ae4669821b72e7940",
                location: form.location,
                interests: interestsIDs,
                languages: form.languages,
                description: form.description,
                duration: form.duration,
                schedule: form.schedule,
                pictures: form.pictures,
            })
            setMessage(response.data.message)
        }
        catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        const interestsList = [];
        props.interests.forEach(element => interestsList.push(element.type))
        setInterests([...interestsList]);
        // eslint-disable-next-line react-hooks/exhaustive-deps  
    }, [props.interests]);


    return <div >
        <Filters className='filtersForm' interests={props.interests} />
    </div>
}
export default ChildProfile;