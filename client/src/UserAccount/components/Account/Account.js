import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import { URL } from '../../../config';
import Activities from '../Activities/Activities';
import ChildProfile from '../ChildProfile/ChildProfile';



const UserAccount = (props) => {
	const [userType, setUserType] = useState('');
	const [userID, setUserID] = useState('');
	const token = JSON.parse(localStorage.getItem('token'))
	useEffect(() => {
		if (token === null) return props.history.push('/')
	}, [token, props.history])

	const verify_token = async () => {
		try {
			const response = await Axios.post(`${URL}/kidder/users/verify_token`, { token })
			return !response.data.ok
				? props.history.push('/')
				: (setUserType(response.data.userType), setUserID(response.data.userID))
		}
		catch (error) {
			console.log(error)
		}
	}
	verify_token()

	let displayFunction = () => {
		var display;
		if (userType === 'business') display = <Activities interests={props.interests} userID={userID} userType = {userType} />
		if (userType === 'parent') display = <ChildProfile interests={props.interests} userID={userID} userType = {userType} />
		if (userType === 'admin') display = <div> <Activities interests={props.interests} userID={userID} userType = {userType} /><ChildProfile interests={props.interests} userID={userID} userType = {userType} /></div>
		return display;
	}
	return <div className='user-account'>
		<h1>This is the User Account page</h1>
		<h2>You can access here only after verifying the token</h2>
		{displayFunction()}
		<button onClick={() => { localStorage.removeItem('token'); props.history.push('/') }}>logout</button>
	</div>
}

export default UserAccount










