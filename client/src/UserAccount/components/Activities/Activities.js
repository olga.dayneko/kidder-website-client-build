import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import Multiselect from 'react-widgets/lib/Multiselect'
import { getIndexByAttribute } from './../../../General/generalMethods'
import {URL} from '../../../config'

const Activities = (props) => {
    var selectedInterestsIDs = [];
    var languages = ['English', 'French', 'Spanish', 'German'];
    const [form, setValues] = useState({
        name: "",
        location: "",
        description: "",
        pictures: "",
        business: props.userID,
        interests: '',
        languages: '',
        duration: '',
        schedule: ''
    });
    const [interests, setInterests] = useState(props.interests);
    const [interestsSelected, setInterestsSelected] = useState([]);
    const [message, setMessage] = useState('');
    var interestsIDs = [];
    const handleChange = e => {
        setValues({ ...form, [e.target.name]: e.target.value })
    }

    const [selectedInterests, setSelectedInterests] = useState([]);

    const onChangeInterests = (value) => {
        setSelectedInterests(value);
    }

    const getInterestsIDs = () => {
        var interestsFromProps = props.interests;
        for (var i = 0; i < selectedInterests.length; i++) {
            var index = getIndexByAttribute(interestsFromProps, 'name', selectedInterests[i])
            selectedInterestsIDs.push(interestsFromProps[index]._id)
        }
        console.log("selectedInterestsIDs: ", selectedInterestsIDs)
        return selectedInterestsIDs;
    }

    useEffect(() => {
        const interestsList = [];
        props.interests.forEach(element => interestsList.push(element.name))
        setInterests([...interestsList]);
        // eslint-disable-next-line react-hooks/exhaustive-deps  
    }, [props.interests]);

    const handleSubmit = async (e) => {
        e.preventDefault()
        getInterestsIDs();
        try {
            const response = await Axios.post(`${URL}/kidder/activities/add`, {
                name: form.activityname,
                business: props.userID,
                location: form.location,
                interests: selectedInterestsIDs,
                languages: form.languages,
                description: form.description,
                duration: form.duration,
                schedule: form.schedule,
                pictures: form.pictures,
            })
            setMessage(response.data.message)

        }
        catch (error) {
            console.log(error)
        }
    }

    const displayBusinessField = () => {
        var display;
        if (!props.userType === 'admin') {
            display = null
        } else {
            display = <div><label>UserID:</label> <input id="userID" type="string" name="userID" /></div>
        }
        return display;
    }
    return <div>
        <form onSubmit={handleSubmit} onChange={handleChange} id="addActivityForm" className="addActivityForm">
            Add activity
            <label>Name of the activity:</label>
            <input id="activityname" type="string" name="activityname" />
            <label>Location:</label>
            <input id="location" type="string" name="location" />
            <label>Description</label>
            <input id="description" type="string" name="description" />
            <label>Interests:</label>
            <Multiselect id="interests"
                name='interests'
                onChange={value => onChangeInterests(value)}
                data={interests}
                allowCreate={true}
                textField='name'
                caseSensitive={false}
                minLength={2}
                filter='contains'
            />
            <label>Languages:</label>
            <Multiselect id="languages"
                data={languages}
                defaultValue={languages[0]}
                textField='language'
                caseSensitive={false}
                minLength={2}
                filter='contains'
            />

            <label>Pictures:</label>
            <input id="pictures" type="string" name="pictures" />
            {displayBusinessField()}
            <label>Duration:</label>
            <input id="duration" type="string" name="duration" />
            <label>Schedule:</label>
            <input id="schedule" type="string" name="schedule" />
            <button>Add</button>
            <div className='message'><h4>{message}</h4></div>
        </form>

    </div>
}
export default Activities;