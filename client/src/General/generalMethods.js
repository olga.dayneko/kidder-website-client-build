import axios from 'axios';

const getItem = (url) => {
    return new Promise((resolve, reject) => {
        axios.get(url).then(res => {
            resolve(res.data);
        }).catch(err => reject(err));
    });
}

const getIndexByAttribute = (array, attr, value)=>{
    for(var i = 0; i < array.length; i += 1) {
        if(array[i][attr] === value) {
            return i;
        }
    }
    return -1;
}
export {getItem, getIndexByAttribute};