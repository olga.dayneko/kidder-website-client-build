import React from 'react'
import { NavLink } from 'react-router-dom'

const Navbar = () => (
  <div className='navbar'>
    <NavLink exact style={styles.default} activeStyle={styles.active} to={"/"}>
      Find your activity
    </NavLink>

    <NavLink exact style={styles.default} activeStyle={styles.active} to={"/registration"}>
      Registration
    </NavLink>

    <NavLink exact style={styles.default} activeStyle={styles.active} to={"/login"}>
      Login
    </NavLink>

    <NavLink exact style={styles.default} activeStyle={styles.active} to={"/account"}>
      Account
    </NavLink>
  </div>
)

export default Navbar

const styles = {
  active: {
    color: "gray"
  },
  default: {
    textDecoration: "none",
    color: "white"
  }
};
